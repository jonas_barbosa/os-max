import { NgModule } from '@angular/core';
import { IonicPageModule } from "ionic-angular";
import { HomePage } from './home';
 '@angular/core';

@NgModule({
 declarations: [HomePage],
 imports: [IonicPageModule.forChild(HomePage)]
})
export class HomeModule{

}